public class Patientsmodel {
    public int Id { get; set; }
    public required string FirstName { get; set; }
    public required string LastName { get; set; }
    public required string Address { get; set; }
    public required string Phone { get; set; }
    public required string Email { get; set; }
    public required string Image { get; set; }
    public DateOnly Dateofbirth { get; set; }
    public required string gender { get; set; }
    
}